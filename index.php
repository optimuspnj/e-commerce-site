<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ShoeBox Online Shopping - Home</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <!--load all styles -->
    <link href="./css/all.min.css" rel="stylesheet"> 
  </head>

  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">ShoeBox.com</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Community</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item mr-1">
              <button type="button" class="btn btn-info"><i class="fas fa-shopping-cart"></i> Cart</button>
            </li>
            <li class="nav-item">
              <button type="button" class="btn btn-success"><i class="fas fa-sign-in-alt"></i> Login</button>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <h1 class="my-4"><i class="fas fa-shoe-prints"></i> Shoe Box</h1>
          <div class="list-group">
            <a href="#" class="list-group-item">Ladies</a>
            <a href="#" class="list-group-item">Gents</a>
            <a href="#" class="list-group-item">Kids</a>
          </div>
        </div>
        <!-- /.col-lg-3 -->
        <div class="col-lg-9 mb-3">
          <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img class="d-block img-fluid" src="./img/1.jpg" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid" src="./img/2.png" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid" src="./img/3.jpg" alt="Third slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <div class="row" id = "result">
            <?php
              #This script loads item details
              $config = parse_ini_file("./ini/db.ini");
              $conn = new mysqli($config["servername"], $config["username"], $config["password"], $config["dbname"]);
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }
              $sql = "SELECT item_name,item_price,item_description,item_rating,item_image FROM item";
              $result = $conn->query($sql);

              $i = 1;

              if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                  if ($i == 7) {
                    break;
                  }
                echo("<div class='col-lg-4 col-md-6 mb-4'>
                  <div class='card h-100'>
                  <a href='#'><img class='card-img-top' src='".$row["item_image"]."' alt=''></a>
                  <div class='card-body'>
                  <h4 class='card-title'>
                  <a href='#'>".$row["item_name"]."</a>
                  </h4>
                  <h5>Rs.".$row["item_price"].".00</h5>
                  <p class='card-text'>".$row["item_description"]."</p>
                  </div>
                  <div class='card-footer'>
                  <small class='text-muted'> &#9733; &#9733; &#9733; &#9733; &#9734; Rating: ".$row["item_rating"]."</small>
                  </div>
                  </div>
                  </div>");
                  $i++;
                }
              } else {
                echo "0 results";
              }
              $conn->close();
            ?>
          <!-- /.row -->
        </div>
        <div class="col-lg-12 text-center">
          <button type="button" class="btn btn-primary" id="loadMoreBtn">Load More</button>
        </div>
        <!-- /.col-lg-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Shoe Box 2021</p>
      </div>
      <!-- /.container -->
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

     <!-- Custom JavaScript -->
     <script src="./js/items_loader.js"></script>
  </body>
</html>

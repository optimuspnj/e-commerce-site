 <?php
    #This script loads item details
    $config = parse_ini_file("./ini/db.ini");
    $conn = new mysqli($config["servername"], $config["username"], $config["password"], $config["dbname"]);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT item_name,item_price,item_description,item_rating,item_image FROM item";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
        echo("<div class='col-lg-4 col-md-6 mb-4'>
            <div class='card h-100'>
            <a href='#'><img class='card-img-top' src='".$row["item_image"]."' alt=''></a>
            <div class='card-body'>
            <h4 class='card-title'>
            <a href='#'>".$row["item_name"]."</a>
            </h4>
            <h5>Rs.".$row["item_price"].".00</h5>
            <p class='card-text'>".$row["item_description"]."</p>
            </div>
            <div class='card-footer'>
            <small class='text-muted'> &#9733; &#9733; &#9733; &#9733; &#9734; Rating: ".$row["item_rating"]."</small>
            </div>
            </div>
            </div>");
        }
        echo("<div class='col-lg-12 col-md-12 mb-4 text-center'><div class='alert alert-danger'><strong>End of the results!</strong></div></div>");
        } else {
            echo "0 results";
        }
    $conn->close();
?>